﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment2.Models;

namespace Assignment2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("Index");
        }
        [HttpPost]
        public ActionResult Index(Credentials checkCre)
        {
            List<Credentials> Login = new List<Credentials>()
            {
                new Credentials(){ user_name="emp1",password="@1"},
                new Credentials(){ user_name="emp2",password="@2"},
                new Credentials(){ user_name="emp3",password="@3"},
                new Credentials(){ user_name="emp4",password="@4"},
                new Credentials(){ user_name="emp5",password="@5"}
            };
            bool a = false;
            foreach (var item in Login)
            {
                if (checkCre.user_name == item.user_name)
                    if (checkCre.password == item.password)
                    {
                        a = true;
                    }
            }
            if(a)
                return View("Order");
            else
                return View("Index");
        }

        public ActionResult Order() 
        {
            return View("Order");
        }
        public ActionResult Result()
        {
            return View("ThankYou");
        }
    }
}