﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment2.Models
{
    public class Credentials
    {
        public string user_name { get; set; }
        public string password { get; set; }
    }
    public class OrderDetails
    {
        public DateTime dob { get; set; }
        public double amount { get; set; }
        [Range(1,9)]
        public int orders { get; set; }
        public double total { get; set; }
    }
}